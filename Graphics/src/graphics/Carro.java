/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Carro {

    private int x;
    private int y;
    private Color colPri;
    private Color colSec;
    private boolean encendido;
    private int direccion;
    private int velocidad;

    public Carro() {
    }

    public Carro(int x, int y, Color colPri, Color colSec) {
        this.x = x;
        this.y = y;
        this.colPri = colPri;
        this.colSec = colSec;
    }

    public void pintar(Graphics g) {
        if (direccion == 0) {
            g.setColor(colPri);
            int[] xs = {x + 30, x + 50, x + 80, x + 140, x + 180, x + 220, x + 220, x + 185, x + 30};
            int[] ys = {y + 30, y + 30, y + 0, y + 0, y + 30, y + 35, y + 60, y + 70, y + 70};
            g.fillPolygon(xs, ys, xs.length);
            g.setColor(Color.black);
            g.fillOval(x + 50, y + 40, 40, 40);
            g.fillOval(x + 145, y + 40, 40, 40);

            g.setColor(colSec);
            int[] xsV = {x + 84, x + 139, x + 175, x + 55};
            int[] ysV = {y + 3, y + 3, y + 30, y + 30};
            g.fillPolygon(xsV, ysV, xsV.length);

            if (encendido) {
                g.setColor(Color.lightGray);
                g.fillOval(x + 20, y + 60, 20, 20);
                g.fillOval(x + 10, y + 50, 20, 20);
                g.fillOval(x + 5, y + 65, 20, 20);
            }
        } else if (direccion == 1) {
           
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColPri() {
        return colPri;
    }

    public void setColPri(Color colPri) {
        this.colPri = colPri;
    }

    public Color getColSec() {
        return colSec;
    }

    public void setColSec(Color colSec) {
        this.colSec = colSec;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

}
