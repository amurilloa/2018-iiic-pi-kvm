/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class MiPanel extends JPanel {

    private Carro carro;
    private Bola[] bolitas;

    public MiPanel() {
        carro = new Carro(0, 400, Color.blue, Color.lightGray);
        bolitas = new Bola[3];
        bolitas[0] = new Bola(10, 10);
        bolitas[1] = new Bola(10, 100);
        bolitas[2] = new Bola(10, 200);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        //Fondo
        g.setColor(Color.white);
        g.fillRect(0, 0, 800, 1000);

        //Circulo de abajo
        g.setColor(Color.black);
        g.fillOval(250, 500, 300, 300);
        g.setColor(Color.white);
        g.fillOval(254, 504, 292, 292);
        g.setColor(Color.black);
        g.setColor(Color.white);
        g.fillArc(255, 700, 300, 200, 0, 180);

        //Dunas
        g.setColor(Color.black);
        g.fillArc(180, 730, 120, 50, 0, 220);
        g.setColor(Color.white);
        g.fillArc(180, 735, 120, 50, 0, 220);
        g.setColor(Color.black);
        g.fillArc(460, 730, 120, 50, 0, 220);
        g.setColor(Color.white);
        g.fillArc(460, 735, 120, 50, 0, 220);
        g.setColor(Color.black);
        g.fillArc(260, 780, 140, 50, 0, 220);
        g.setColor(Color.white);
        g.fillArc(260, 785, 140, 50, 0, 220);

        //Circulo medio
        g.setColor(Color.black);
        g.fillOval(275, 335, 250, 250);
        g.setColor(Color.white);
        g.fillOval(279, 339, 242, 242);
        g.setColor(Color.black);

        //Sombrero
        g.setColor(Color.black);
        g.fillRoundRect(335, 100, 130, 200, 90, 90);
        g.setColor(Color.white);
//        g.fillRoundRect(142, 85, 200, 130, 110, 110);
//        g.fillRoundRect(460, 85, 200, 130, 110, 110);

        //Cabeza
        g.setColor(Color.black);
        g.fillOval(300, 195, 200, 200);
        g.setColor(Color.white);
        g.fillOval(304, 199, 192, 192);
        g.setColor(Color.black);

        //Boca
        g.fillOval(390, 340, 20, 20);
        g.fillOval(360, 335, 20, 20);
        g.fillOval(420, 335, 20, 20);
        g.fillOval(340, 320, 20, 20);
        g.fillOval(440, 320, 20, 20);

        //Nariz
        g.setColor(Color.orange);
        g.fillOval(383, 290, 34, 34);

        //Ojos
        g.setColor(Color.black);

        g.fillOval(340, 240, 40, 45);
        g.fillOval(420, 240, 40, 45);

        //botones
        g.setColor(Color.black);
        g.fillOval(385, 410, 30, 30);
        g.fillOval(385, 460, 30, 30);
        g.fillOval(385, 510, 30, 30);

        //Sombrero
        g.setColor(Color.black);
        g.fillRoundRect(335, 184, 130, 20, 10, 10);
        g.setColor(Color.gray);
        g.fillRoundRect(339, 188, 122, 12, 10, 10);

        g.setColor(Color.black);
        g.fillRoundRect(376, 170, 44, 44, 10, 10);
        g.setColor(Color.yellow);
        g.fillRoundRect(380, 174, 36, 36, 10, 10);

        g.setColor(Color.black);
        g.fillRoundRect(385, 180, 26, 26, 10, 10);
        g.setColor(Color.gray);
        g.fillRoundRect(389, 184, 18, 18, 10, 10);

        g.setColor(Color.black);
        g.fillRoundRect(275, 200, 250, 20, 10, 10);
        g.setColor(Color.gray);
        g.fillRoundRect(279, 204, 242, 12, 10, 10);

        //Manos
//        Color cafe = new Color(78, 50, 50);
//        int[] xs = {};
//        int[] ys = {};
//        g.fillPolygon(xs, ys, xs.length);
//        carro.setDireccion(0);
//        carro.setEncendido(true);
//        carro.pintar(g);
        for (Bola bolita : bolitas) {
            bolita.pintar(g);
            bolita.mover();
            bolita.rebotar(getWidth(), getHeight());
        }

        //Guia 
        g.setColor(Color.black);
        g.drawLine(400, 0, 400, 1000);
        g.drawLine(0, 400, 800, 400);

    }

}
