/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Bola {

    private int x;
    private int y;
    private int dir;

    public Bola() {
    }

    public Bola(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void pintar(Graphics g) {
        g.setColor(Color.black);
        g.fillOval(x, y, 50, 50);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void mover() {
        if (dir == 0) {
            x += 5;
        } else if (dir == 1) {
            x -= 5;
        } else if (dir == 2) {
            y += 5;
        } else if (dir == 3) {
            y -= 5;
        } else if (dir == 4) {
            x += 5;
            y += 5;
        } else if (dir == 5) {
            x -= 5;
            y += 5;
        }
    }

    public void rebotar(int limX, int limY) {
        if (x >= limX - 50) {
            dir = 1;
        } else if (x <= 0) {
            dir = 4;
        } else if (y >= limY) {
            dir = 3;
        } else if (y <= 0) {
            dir = 5;
        }
    }

}
