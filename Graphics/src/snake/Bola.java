/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author ALLAN
 */
public class Bola {

    private Color color;
    private int tamanno;

    private int x;
    private int y;

    public Bola(Color color, int tamanno, int x, int y) {
        this.color = color;
        this.tamanno = tamanno;
        this.x = x;
        this.y = y;
    }
    
    public Rectangle getBounds(){
        Rectangle temp = new Rectangle(x, y, tamanno, tamanno);
        return temp;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, tamanno, tamanno);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getTamanno() {
        return tamanno;
    }

    public void setTamanno(int tamanno) {
        this.tamanno = tamanno;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
