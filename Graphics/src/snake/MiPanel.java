/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class MiPanel extends JPanel implements KeyListener {

    private Snake culebra;

    public MiPanel() {
        culebra = new Snake();
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //Fondo
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 1200, 800);
        //Juego
        culebra.pintar(g);
        culebra.mover();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1200, 800);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
//        System.out.println("Escribi");
//        System.out.println("Typed: " + ke.getKeyChar());
//        System.out.println("Typed: " + ke.getKeyCode());
    }

    @Override
    public void keyPressed(KeyEvent ke) {
//        System.out.println("presione");
//        System.out.println("Pressed: " + ke.getKeyChar());
//        System.out.println("Pressed: " + ke.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent ke) {
//        System.out.println("Solte");
//        System.out.println("Released: " + ke.getKeyChar());
        System.out.println("Released: " + ke.getKeyCode());
        culebra.cambiarDir(ke.getKeyCode());
    }

}
