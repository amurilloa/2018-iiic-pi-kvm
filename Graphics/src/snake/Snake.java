/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Snake {

    private Bola manzana;
    private LinkedList<Bola> snake;
    private int direccion; //1 der, 2 izq, 3 arriba, 4 abajo

    public Snake() {
        snake = new LinkedList<>();
        config();
    }

    private void config() {
        //Manzana
        manzana = new Bola(Color.GREEN, 30, 455, 420);
        //Culebra incial
        snake.add(new Bola(Color.red, 30, 450, 480));
        snake.add(new Bola(Color.white, 30, 450, 510));
        snake.add(new Bola(Color.white, 30, 450, 540));
    }

    public void pintar(Graphics g) {
        manzana.pintar(g);
        for (Bola bola : snake) {
            bola.pintar(g);
        }
    }

    public void mover() {
        Bola cab = snake.getFirst();
        //if (cab.getX() == manzana.getX() && cab.getY() == manzana.getY()) {
        if (cab.getBounds().intersects(manzana.getBounds())) {
            System.out.println("Por encima de la manzana");
            manzana.setX((int) (Math.random() * 1200));
            manzana.setY((int) (Math.random() * 800));
        } else if (direccion > 0) {
            snake.removeLast();
        }

        if (direccion == 1) {
            snake.getFirst().setColor(Color.white);
            Bola nueva = new Bola(Color.red, 30,
                    snake.getFirst().getX() + 30,
                    snake.getFirst().getY());
            snake.addFirst(nueva);
        } else if (direccion == 2) {
            snake.getFirst().setColor(Color.white);
            Bola nueva = new Bola(Color.red, 30,
                    snake.getFirst().getX() - 30,
                    snake.getFirst().getY());
            snake.addFirst(nueva);
        } else if (direccion == 3) {
            snake.getFirst().setColor(Color.white);
            Bola nueva = new Bola(Color.red, 30, snake.getFirst().getX(),
                    snake.getFirst().getY() - 30);
            snake.addFirst(nueva);
        } else if (direccion == 4) {
            snake.getFirst().setColor(Color.white);
            Bola nueva = new Bola(Color.red, 30,
                    snake.getFirst().getX(),
                    snake.getFirst().getY() + 30);
            snake.addFirst(nueva);
        }
    }

    public void cambiarDir(int keyCode) {
        switch (keyCode) {
            case 37:
                direccion = 2;
                break;
            case 38:
                direccion = 3;
                break;
            case 39:
                direccion = 1;
                break;
            case 40:
                direccion = 4;
                break;
        }
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

}
