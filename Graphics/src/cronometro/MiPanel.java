/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class MiPanel extends JPanel implements KeyListener {

    private Cronometro cro;

    public MiPanel() {
        setFocusable(true);
        addKeyListener(this);
        cro = new Cronometro();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g); //To change body of generated methods, choose Tools | Templates.
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
        cro.incrementarTiempo(200);
        cro.pintar(g);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(850, 800);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == 73) {
            cro.iniciar();
        } else if (ke.getKeyCode() == 83) {
            cro.detener();
        } else if (ke.getKeyCode() == 82) {
            cro.reiniciar();
        } else if (ke.getKeyCode() == 80) {
           cro.parcial();
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
