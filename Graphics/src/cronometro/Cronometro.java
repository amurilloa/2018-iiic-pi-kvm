/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Cronometro {

    private int ms;
    private int seg;
    private int min;
    private int hor;
    private boolean iniciado;
    private LinkedList<String> tiempos;

    public Cronometro() {
        tiempos = new LinkedList<>();
    }

    public void incrementarTiempo(int ms) {
        if (iniciado) {
            this.ms += ms;
        }
        //10009322/1000/60/60=2
        int milSeg = this.ms;
        hor = milSeg / 1000 / 60 / 60;

        milSeg = milSeg - (hor * 60 * 60 * 1000);
        min = milSeg / 1000 / 60;

        milSeg = milSeg - (min * 60 * 1000);
        seg = milSeg / 1000;
    }

    public void pintarNumero(Graphics g, int num, int x, int y) {
        g.setColor(Color.WHITE);

        if (num != 4 && num != 1) {
            g.fillRect(x, y, 80, 10);
        }

        if (num != 0 && num != 1 && num != 7) {
            g.fillRect(x, y + 80, 80, 10);
        }
        if (num != 1 && num != 4 && num != 7) {
            g.fillRect(x, y + 160, 80, 10);
        }

        if (num == 4 || num == 5 || num == 6 || num == 8 || num == 9 || num == 0) {
            g.fillRect(x, y, 10, 80);
        }

        if (num != 5 && num != 6) {
            g.fillRect(x + 70, y, 10, 80);
        }

        if (num == 2 || num == 6 || num == 8 || num == 0) {
            g.fillRect(x, y + 80, 10, 80);
        }
        if (num != 2) {
            g.fillRect(x + 70, y + 80, 10, 80);
        }
    }

    public void pintar(Graphics g) {

        int uh = hor % 10;
        int dh = hor / 10;

        pintarNumero(g, dh, 100, 100);
        pintarNumero(g, uh, 200, 100);

        int um = min % 10;
        int dm = min / 10;

        pintarNumero(g, dm, 330, 100);
        pintarNumero(g, um, 430, 100);

        int us = seg % 10;
        int ds = seg / 10;

        pintarNumero(g, ds, 560, 100);
        pintarNumero(g, us, 660, 100);

        //Pintar tiempos
        g.setFont(new Font("Arial", Font.PLAIN, 64));
        int i = 1;
        int y = 320;
        for (String tiempo : tiempos) {
            g.drawString("#" + i + " >>> " + tiempo, 250, y + (60 * i));
            i++;
        }

    }

    public void iniciar() {
        iniciado = true;
    }

    public void detener() {
        iniciado = false;
    }

    public void reiniciar() {
        ms = 0;
        tiempos.clear();
    }

    public void parcial() {
        if(!iniciado){
            return;
        }
        String h = String.format("%02d", hor);
        String m = String.format("%02d", min);
        String s = String.format("%02d", seg);
        if (tiempos.size() >= 7) {
            tiempos.removeFirst();
        }
        tiempos.add(h + ":" + m + ":" + s);
    }
}
