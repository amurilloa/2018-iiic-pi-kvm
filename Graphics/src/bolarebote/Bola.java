/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolarebote;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Bola {

    private Color color;
    private int tamanno;

    private int x;
    private int y;
    private int dir;

    public Bola(Color color, int tamanno, int x, int y) {
        this.color = color;
        this.tamanno = tamanno;
        this.x = x;
        this.y = y;
        dir = 1;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, tamanno, tamanno);
    }

    public void mover() {
        if (dir == 0) {
            y += 5;
        } else if (dir == 1) {
            y -= 5;
        } else if (dir == 2) {
            x += 5;
        } else if (dir == 3) {
            x -= 5;
        } else if (dir == 4) {
            y += 5;
            x += 5;
        } else if (dir == 5) {
            y -= 5;
            x -= 5;
        } else if (dir == 6) {
            y += 5;
            x -= 5;
        } else if (dir == 7) {
            y -= 5;
            x += 5;
        }
    }

    public void rebotar(int limX, int limY) {
        if (y <= 0) {
            int op = (int) (Math.random() * 3) + 1;
            dir = op == 1 ? 6 : op == 2 ? 0 : 4;
        } else if (y >= limY - tamanno) {
            int op = (int) (Math.random() * 3) + 1;
            dir = op == 1 ? 5 : op == 2 ? 1 : 7;
        } else if (x <= 0) {
            int op = (int) (Math.random() * 3) + 1;
            dir = op == 1 ? 7 : op == 2 ? 2 : 4;
        } else if (x >= limX - tamanno) {
            int op = (int) (Math.random() * 3) + 1;
            dir = op == 1 ? 5 : op == 2 ? 3 : 6;
        }
    }

}
