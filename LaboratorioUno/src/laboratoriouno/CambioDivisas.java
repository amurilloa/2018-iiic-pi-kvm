/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class CambioDivisas {

    private double colones;
    private double tipoCambio;

    public CambioDivisas(double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public double convertirADolares() {
        return colones / tipoCambio;
    }

    public double getColones() {
        return colones;
    }

    public void setColones(double colones) {
        this.colones = colones;
    }

    public double getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

}
