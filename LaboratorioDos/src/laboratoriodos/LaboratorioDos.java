/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriodos;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class LaboratorioDos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String titulo = "Pintor UTN - v0.1";
        String menu1 = "Pintor UTN - v0.1\n"
                + "1. Nueva cotización\n"
                + "2. Salir";

        String menu2 = "Pintor UTN - v0.1\n"
                + "Desea agregar otra pared\n"
                + "1. SI\n"
                + "2. NO";

        String menu3 = "Pintor UTN - v0.1\n"
                + "Desea agregar una/otra ventana a la pared #%d\n"
                + "1. SI\n"
                + "2. NO";

        String menu4 = "Pintor UTN - v0.1\n"
                + "Tipo de la ventana #%d\n"
                + "1. Rectangular/Cuadrada\n"
                + "2. Circular";

        //Nombre, datos pared, tiene ventanas, tipo de ventana, si tine más paredes
        while (true) {
            int op = Util.leerIntJOP(menu1);
            if (op == 2) {
                Util.mostrarJOP("Gracias por utilizar la aplicación.", titulo);
                break;
            } else if (op == 1) { //Nueva cotización 
                String nombre = Util.leerStringJOP("Nombre del Cliente");
                PintorUTN pintor = new PintorUTN();
                pintor.setNombre(nombre);
                int canParedes = 1;
                while (true) {
                    double largo = Util.leerDoubleJOP("Alto de la pared #" + canParedes);
                    double ancho = Util.leerDoubleJOP("Ancho de la pared #" + canParedes);
                    pintor.agregarPared(largo, ancho);
                    //Ventanas
                    int cantVentanas = 1;
                    while (true) {
                        op = Util.leerIntJOP(String.format(menu3, canParedes));
                        if (op == 2) {
                            break;
                        }
                        op = Util.leerIntJOP(String.format(menu4, cantVentanas));
                        if (op == 1) {
                            largo = Util.leerDoubleJOP("Alto de la ventana #" + cantVentanas);
                            ancho = Util.leerDoubleJOP("Ancho de la ventana #" + cantVentanas);
                            pintor.agregarVentana(largo, ancho);
                        } else {
                            double diametro = Util.leerDoubleJOP("Ancho(diametro) de la ventana #" + cantVentanas);
                            pintor.agregarVentana(diametro);
                        }
                    }

                    op = Util.leerIntJOP(menu2);
                    if (op == 2) {
                        break;
                    }
                    canParedes++;
                }
                //Mostrar resultados
                Util.mostrarJOP(pintor.cotizar(), titulo);
            }
        }
    }

}
