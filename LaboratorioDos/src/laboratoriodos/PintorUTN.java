/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriodos;

/**
 *
 * @author ALLAN
 */
public class PintorUTN {

    private String nombre;
    private double tParedes;
    private double tVentanas;

    private final int TIEMPO_X_METRO = 10;
    private final int COSTO_X_HORA_USD = 30;

    public void agregarPared(double largo, double ancho) {
        Rectangulo pared = new Rectangulo(largo, ancho);
        tParedes += pared.calcularArea();
    }

    public void agregarVentana(double largo, double ancho) {
        Rectangulo ventana = new Rectangulo(largo, ancho);
        tVentanas += ventana.calcularArea();
    }

    public void agregarVentana(double diametro) {
        Circunferencia ventana = new Circunferencia(diametro / 2);
        tVentanas += ventana.calcularArea();
    }

    public String cotizar() {
        String resultados = "Pintor UTN - v0.1\n"
                + "Cliente: %s\n\n"
                + "Total de área de paredes:  %.2f\n"
                + "Total de área de ventanas: %.2f\n"
                + "Total de área a pintar:    %.2f\n\n"
                + "Tiempo aproximado: %ddías, %dhrs\n"
                + "Costo Total: $%d";
        double tPintar = tParedes - tVentanas;
        int tMin = (int) (Math.round(tPintar + 0.5) * TIEMPO_X_METRO);
        int tHrs = (int) Math.round(tMin / 60.0 + 0.5);

        int d = tHrs / 24;
        int hrs = tHrs % 24;
        int costo = tHrs * COSTO_X_HORA_USD;
        return String.format(resultados, nombre, tParedes, tVentanas, tPintar, d, hrs, costo);
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
