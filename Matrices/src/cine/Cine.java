/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cine;

/**
 *
 * @author ALLAN
 */
public class Cine {
    
    private Asiento[][] asientos;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    public Cine() {
        asientos = new Asiento[9][9];
    }

    public void generarAsientos() {
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                int precio = f < 3 ? 2000 : f < 6 ? 3000 : 4000;
                asientos[f][c] = new Asiento(String.valueOf((char) (f + 65)), c + 1, precio);
            }
        }
    }

    public String verAsientos() {
        String str = "";
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (!asientos[f][c].isEstado()) {
                    str += ANSI_GREEN;
                } else {
                    str += ANSI_RED;
                }
                str += asientos[f][c].getNombre() + " ";
                str += ANSI_RESET;
            }
            str += "\n";
        }
        return str;
    }

    public boolean venderAsiento(String numero) {
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (numero.equalsIgnoreCase(asientos[f][c].getNombre())) {
                    if (!asientos[f][c].isEstado()) {
                        asientos[f][c].setEstado(true);
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public String disponibilidad() {
        String info = "Tipo 1: %d/27\n"
                + "Tipo 2: %d/27\n"
                + "Tipo 3: %d/27\n"
                + " Total: %d/81";
        int p1 = 0;
        int p2 = 0;
        int p3 = 0;
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                Asiento temp = asientos[f][c];
                if (temp.getPrecio() == 2000 && temp.isEstado()) {
                    p1++;
                } else if (temp.getPrecio() == 3000 && temp.isEstado()) {
                    p2++;
                } else if (temp.isEstado()) {
                    p3++;
                }
            }
        }

        return String.format(info, p1, p2, p3, (p1 + p2 + p3));
    }

    public int taquilla() {
        int total = 0;
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (asientos[f][c].isEstado()) {
                    total += asientos[f][c].getPrecio();
                }
            }
        }
        return total;
    }

}
