/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private int[][] matriz;

    public Logica(int filas, int columnas) {
        matriz = new int[filas][columnas];
    }

    public void llenarMatriz() {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = (int) (Math.random() * 20) + 10;
            }
        }
    }

    public String imprimir() {
        String txt = "";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                txt += matriz[i][j] + ", ";
            }
            txt += "\b\b\n";
        }
        return txt;
    }

    public int[][] getMatriz() {
        return matriz;
    }
}
