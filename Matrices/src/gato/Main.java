/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {

        for (int i = 0; i < 27; i++) {
            System.out.println((char) (65 + i));
        }

        Logica log = new Logica();
        //System.out.println((int) (Math.random() * 10) + 1);

        while (true) {
            //Imprimir el estado del juego    
            System.out.println(log.imprimir());
            //Pedir los datos 
            int f = Util.leerInt("Fila");
            int c = Util.leerInt("Columna");
            //Si no pudo jugar
            if (!log.jugar(f, c)) {
                System.out.println("Intente nuevamente");
            }
            //Ganó ??  Empató ??
            if (log.gano()) {
                System.out.println(log.imprimir());
                System.out.println("Felicidades..." + log.ganador());
                break;
            } else if (log.empato()) {
                System.out.println(log.imprimir());
                System.out.println("Juego empatado");
                break;
            }
        }
    }
}
