/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private char[][] gato;
    private boolean turno;

    public Logica() {
        gato = new char[3][3];
        turno = true;
    }

    public String imprimir() {
        String str = "";
        for (int f = 0; f < gato.length; f++) {
            for (int c = 0; c < gato[f].length; c++) {
                str += gato[f][c] == '\u0000' ? "_ " : gato[f][c] + " ";
            }
            str += "\n";
        }
        return str;
    }

    public boolean jugar(int f, int c) {
        if (gato[f - 1][c - 1] == '\u0000') {
            gato[f - 1][c - 1] = turno ? 'X' : 'O';
            turno = !turno;
            return true;
        }
        return false;
    }

    public boolean gano() {
        //Si ganó en una fila
        for (int f = 0; f < gato.length; f++) {
            if (gato[f][0] == gato[f][1] && gato[f][0] == gato[f][2] && gato[f][0] != '\u0000') {
                return true;
            }
        }

        for (int c = 0; c < gato[0].length; c++) {
            if (gato[0][c] == gato[1][c] && gato[0][c] == gato[2][c] && gato[0][c] != '\u0000') {
                return true;
            }
        }

        if (gato[0][0] == gato[1][1] && gato[0][0] == gato[2][2] && gato[0][0] != '\u0000') {
            return true;
        }

        if (gato[2][0] == gato[1][1] && gato[2][0] == gato[0][2] && gato[2][0] != '\u0000') {
            return true;
        }

        return false;
    }

    public boolean empato() {
        for (int f = 0; f < gato.length; f++) {
            for (int c = 0; c < gato[f].length; c++) {
                if (gato[f][c] == '\u0000') {
                    return false;
                }
            }
        }
        return true;
    }

    public String ganador() {
        return "Gana el jugador con letra " + (turno ? 'O' : 'X');
    }
}
