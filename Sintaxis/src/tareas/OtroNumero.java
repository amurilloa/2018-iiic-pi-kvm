/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareas;

/**
 *
 * @author ALLAN
 */
public class OtroNumero {

    private int numero;

    public OtroNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Determina si un número es primo o compuesto
     *
     * @return boolean true si es primo, false si es compuesto
     */
    public boolean esPrimo() {
        int div = 0;
        for (int i = 1; i <= numero; i++) {
            if (numero % i == 0) {
                div++;
            }
            if (div > 2) {
                return false;
            }
        }
        return div == 2;
    }

    /**
     * Determina si un número es perfecto, abundante o deficiente
     *
     * @return int 1 - Perfecto, 2-Deficiente, 3-Abundante
     */
    public int tipoNumero() {
        int sumDiv = 0;
        for (int i = 1; i < numero; i++) {
            if (numero % i == 0) {
                sumDiv += i;
            }
        }
        return sumDiv == numero ? 1 : sumDiv < numero ? 2 : 3;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
