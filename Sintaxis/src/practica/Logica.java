/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     * Determina sin un número es negativo o positivo, además indica si el mismo
     * es divisible por 5
     *
     * @param num número a evaluar
     * @return texto indicando lo anteriors
     */
    public String ejercicioUno(int num) {
        String res = "";
        if (num < 0) {
            res += "Es negativo";
        } else {
            res += "Es positivo";
        }
        if (num % 5 == 0) {
            res += "\nEs divisible entre 5";
        }
        return res;
    }

    /**
     * Genera un número aleatorio entre 1 y 6
     *
     * @return numero aleatorio
     */
    public int ejercicioDos() {
        int max = 6;
        int min = 1;
        int numAle = (int) (Math.random() * max) + min;
        return numAle;
    }

    /**
     * Determina el costo final del vehículo con impuestos
     *
     * @param pasajeros capacidad de ocupantes del vehículo
     * @param costo costo sin impuestos
     * @param ejes cantidad de ejes del vehículo
     * @return costo final del vehículo
     */
    public double ejercicioTres(int pasajeros, int costo, int ejes) {
        double monImp = costo * 0.01;
        double costoTotal = costo + monImp;
        if (pasajeros < 20) {
            costoTotal += monImp * 0.01;
        } else if (pasajeros >= 20 && pasajeros <= 60) {
            costoTotal += monImp * 0.05;
        } else {
            costoTotal += monImp * 0.08;
        }

        if (ejes == 2) {
            costoTotal += monImp * 0.05;
        } else if (ejes == 3) {
            costoTotal += monImp * 0.10;
        } else if (ejes > 3) {
            costoTotal += monImp * 0.15;
        }
        return costoTotal;
    }

    /**
     * Convertir de una nota cuantitativa a una cualitativa
     *
     * @param nota nota a evaluar
     * @return nota cualitativa
     */
    public String ejercicioCuatro(int nota) {
//        if (nota >= 90) {
//            return "Sobresaliente";
//        } else if (nota >= 80) {
//            return "Notable";
//        } else if (nota >= 70) {
//            return "Bien";
//        } else {
//            return  "Insuficiente";
//        }
        //Alternativa
        if (nota < 70) {
            return "Insuficiente";
        } else if (nota < 80) {
            return "Bien";
        } else if (nota < 90) {
            return "Notable";
        } else {
            return "Sobresaliente";
        }
    }

    /**
     * Determina el nivel del músico a partir de su cantidad de canciones y
     * partituras
     *
     * @param can Cantidad de Canciones
     * @param par Cantidad de Partituras
     * @return Nivel del músico
     */
    public String ejercicioCinco(int can, int par) {
//        if (can >= 7 && can <= 10 && par == 0) {
//            return "Músico Naciente";
//        } else if (can >= 7 && can <= 10 && par >= 1 && par <= 5) {
//            return "Músico Estelar";
//        } else if (can > 10 && par > 5) {
//            return "Músico consagrado";
//        } else {
//            return "Músico en formación";
//        }
        if (can > 10 && par > 5) {
            return "Músico consagrado";
        } else if (can >= 7 && can <= 10) {
            if (par == 0) {
                return "Músico Naciente";
            } else if (par >= 1 && par <= 5) {
                return "Músico Estelar";
            } else {
                return "Músico en formación";
            }
        } else {
            return "Músico en formación";
        }
    }

    /**
     * Desglosa la cantidad de billetes de cada denominación a entregar para un 
     * monto específico
     * @param monto valor a retirar 
     * @return desglose de billetes y monedas
     */
    public String ejercicioSeis(int monto) {
        String res = "";
        int moneda = 500;

        if (monto >= moneda) {
            int b = monto / moneda;
            res += b + " billetes de " + moneda + "\n";
            monto %= moneda;
            //monto -= moneda * b; 
        }

        moneda = 200;
        if (monto >= moneda) {
            int b = monto / moneda;
            res += b + " billetes de " + moneda + "\n";
            monto %= moneda;
        }

        moneda = 100;
        if (monto >= moneda) {
            int b = monto / moneda;
            res += b + " billetes de " + moneda + "\n";
            monto %= moneda;
        }

        moneda = 50;
        if (monto >= moneda) {
            int b = monto / moneda;
            res += b + " billetes de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 20;
        if (monto >= moneda) {
            int b = monto / moneda;
            res += b + " billetes de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 10;
        if (monto >= moneda) {
            int b = monto / moneda;
            res += b + " billetes de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 5;
        if (monto >= moneda) {
            int b = monto / moneda;
            res += b + " billetes de " + moneda + "\n";
            monto %= moneda;
        }

        moneda = 2;
        if (monto >= moneda) {
            int b = monto / moneda;
            res += b + " monedas de " + moneda + "\n";
            monto %= moneda;
        }

        if (monto > 0) {
            res += monto + " monedas de 1";
        }
        return res;
    }
}
