/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

/**
 *
 * @author ALLAN
 */
public class Logica {
    
        
    //nivel acceso + tipo de retorno + nombre metodo (parametros*)
    /**
     * Sumar dos números enteros
     *
     * @param num1 numero 1
     * @param num2 numero 2
     * @return Resultado de sumar numero 1 + numero 2
     */
    public static int sumar(int num1, int num2) {
        return num1 + num2;
    }

    /**
     * Resta dos números enteros
     *
     * @param num1 numero 1
     * @param num2 numero 2
     * @return Resutaldo de restar numero 1 - numero 2
     */
    public int restar(int num1, int num2) {
        return num1 - num2;
    }

    /**
     * Multiplica dos números enteros
     *
     * @param num1 numero 1
     * @param num2 numero 2
     * @return Resutaldo de multiplicar numero 1 * numero 2
     */
    public int multiplicar(int num1, int num2) {
        return num1 * num2;
    }

    /**
     * Divbde dos números enteros
     *
     * @param num1 numero 1
     * @param num2 numero 2
     * @return Resutaldo de dividir numero 1 / numero 2
     */
    public int dividir(int num1, int num2) {
        return num1 / num2;
    }
}
