/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class EstructurasControl {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Digite un número: ");
        int num = Integer.parseInt(sc.nextLine());
   
        //int num = Integer.parseInt(JOptionPane.showInputDialog("Digite un número: "));
        if (num % 2 == 0) {
            System.out.println("Es par");
        } else {
            System.out.println("Es impar");
        }
        
        String menu = "Menu Prueba\n"
                + "1. Opción 1\n"
                + "2. Opción 2\n"
                + "3. Salir\n"
                + "Seleccione una opción: ";

        System.out.print(menu);
        int op = Integer.parseInt(sc.nextLine());
        // int op = Integer.parseInt(JOptionPane.showInputDialog(menu));

        System.out.println(op);
        if (op == 1) {
            System.out.println("Entró en la opción 1");
        } else if (op == 2) {
            System.out.println("Entró en la opción 2");
        } else if (op == 3) {
            System.out.println("Gracias por utilizar la aplicación xD");
        } else {
            System.out.println("Seleccionó otra opción");
        }
        
        
        
        
        
        

    }
}
