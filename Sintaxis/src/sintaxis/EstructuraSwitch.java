/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructuraSwitch {

    public static String meses(int mes) {
        String res = "";
        switch (mes) {
            case 1:
                res = "Enero";
                break;
            case 2:
                res = "Febrero";
                break;
            case 3:
                res = "Marzo";
                break;
            case 4:
                res = "Abril";
                break;
            case 5:
                res = "Mayo";
                break;
            case 6:
                res = "Junio";
                break;
            case 7:
                res = "Julio";
                break;
            case 8:
                res = "Agosto";
                break;
            case 9:
                res = "Septiembre";
                break;
            case 10:
                res = "Octubre";
                break;
            case 11:
                res = "Noviembre";
                break;
            case 12:
                res = "Diciembre";
                break;
            default:
                res = "Número de mes inválido";
        }
        return res;

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
//        System.out.print("Digite una opción: ");
//        int op = Integer.parseInt(sc.nextLine());
//        switch (op) {
//            case 1:
//                System.out.println("Es un uno xD");
//                break;
//            case 2:
//                System.out.println("Es un dos xD");
//                System.out.println("Es un dos xD");
//                break;
//            case 3:
//                System.out.println("Es un tres xD");
//                System.out.println("Es un tres xD");
//                System.out.println("Es un tres xD");
//                break;
//            default:
//                System.out.println("Es otro número");
//                break;
//        }

        System.out.print("Digite el número de día entre 1-7: ");
        int dia = Integer.parseInt(sc.nextLine());
        switch (dia) {
            case 1:
                System.out.println("Domingo");
                break;
            case 2:
                System.out.println("Lunes");
                break;
            case 3:
                System.out.println("Martes");
                break;
            case 4:
                System.out.println("Miércoles");
                break;
            case 5:
                System.out.println("Jueves");
                break;
            case 6:
                System.out.println("Viernes");
                break;
            case 7:
                System.out.println("Sábado");
                break;
            default:
                System.out.println("Número de día incorrecto");
        }

        System.out.println(meses(12));
        
    }
}
