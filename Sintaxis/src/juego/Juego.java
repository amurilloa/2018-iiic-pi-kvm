/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author ALLAN
 */
public class Juego {

    private int intentos;
    private int aleatorio;
    private boolean gano;

    public Juego() {
        intentos = 5;
        aleatorio = (int) (Math.random() * 10) + 1;
    }

    /**
     * Juega uno de los intentos del usuario
     * @param num número digitado por el usuario
     */
    public void jugar(int num) {
        gano = num == aleatorio;
        intentos--;
    }

    public boolean perdio() {
        return intentos == 0;
    }

    public String pista(int num) {
        return num < aleatorio ? "Mayor" : "Menor";
    }

    public int getIntentos() {
        return intentos;
    }

    public int getAleatorio() {
        return aleatorio;
    }

    public boolean isGano() {
        return gano;
    }

}
