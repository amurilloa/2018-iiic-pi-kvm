/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garrobo;

/**
 *
 * @author ALLAN
 */
public class Garrobo {

    public String nombre;
    public double distancia;
    public double tiempo;

    public Garrobo() {
    }

    public Garrobo(String nombre, double distancia, double tiempo) {
        this.nombre = nombre;
        this.distancia = distancia;
        this.tiempo = tiempo;
    }

    /**
     * Calcula cuanta distancia recorre un garrobo en tiempo determinado
     *
     * @param t Tiempo a calcular
     * @return Distancia rrecorrida por el garrobo
     */
    public double calcularDistancia(double t) {
        double velocidad = calcularVelocidad();
        double dist = velocidad * t;
        return dist;
    }

    /**
     *  Calcula cuanto tiempo dura el garrobo recorriendo una distancia
     * @param d Distancia a evaluar
     * @return Tiempo estimado
     */
    public double calcularTiempo(double d) {
        double velocidad = calcularVelocidad();
        double tiem = d / velocidad;
        return tiem;
    }

    /**
     * Calcula la velocidad del garrobo con la configuración actual
     * @return velocidad actual del garroo
     */
    private double calcularVelocidad() {
        return distancia / tiempo;
    }
}
