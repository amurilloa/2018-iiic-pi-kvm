/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     * Suma dos números enteros
     *
     * @param n1 int numero 1
     * @param n2 int numero 2
     * @return suma de n1 con n2
     */
    public int sumar(int n1, int n2) {
        return n1 + n2;
    }

    /**
     * Resta dos números enteros
     *
     * @param n1 int numero 1
     * @param n2 int numero 2
     * @return resta de n1 con n2
     */
    public int restar(int n1, int n2) {
        return n1 - n2;
    }

    /**
     * Multiplica dos números enteros
     *
     * @param n1 int numero 1
     * @param n2 int numero 2
     * @return multiplicación de n1 con n2
     */
    public int multiplicar(int n1, int n2) {
        return n1 * n2;
    }

    /**
     * Divide dos números enteros
     *
     * @param n1 int numero 1
     * @param n2 int numero 2
     * @return división de n1 con n2
     */
    public double dividir(int n1, int n2) {
        if (n2 != 0) {
            return n1 / (double) n2;
        } else {
            return 0;
        }
    }
}
