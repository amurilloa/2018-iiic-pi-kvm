/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iexamenallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Logica {

    public String imprimir(int[] arreglo) {
        String txt = "";
        for (int i = 0; i < arreglo.length; i++) {
            txt += arreglo[i] + ", ";
        }
        return txt + "\b\b";
    }

    public int[] crearArreglo(int i, int f) {
        int[] temp = new int[Math.abs(f - i) + 1];

        if (i < f) {
            int con = i;
            for (int j = 0; j < temp.length; j++) {
                temp[j] = con++;
            }
        } else {
            int con = i;
            for (int j = 0; j < temp.length; j++) {
                temp[j] = con--;
            }
        }
        return temp;
    }

    public int sumarImpares(int n, int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                suma += arreglo[i];
                n--;
                if (n == 0) {
                    return suma;
                }
            }
        }
        return suma;
    }

    public int cantidadBisiestos(int[] annos) {
        int con = 0;
        for (int i = 0; i < annos.length; i++) {
            if (esBisiesto(annos[i])) {
                con++;
            }
        }
        return con;
    }

    private boolean esBisiesto(int anno) {
        if (anno % 4 == 0) {
            if (anno % 100 == 0) {
                return anno % 400 == 0;
            }
            return true;
        }
        return false;
    }
}
