/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iexamenallanmurillo;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class IExamenAllanMurillo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();
        int[] arreglo = {1, 2, 3, 4, 5, 3, 5, 6, 7, 2};
        System.out.println(log.imprimir(arreglo));
        int[] annos = {2000, 2001, 2002, 2003, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012};

        //Menu 
        String menu = "Menu Principal\n"
                + "i.   Imprimir números\n"
                + "ii.  Suma de Impares\n"
                + "iii. Cantidad Bisiestos\n"
                + "iv.  Salir\n"
                + "Seleccione una opción: ";

        OUTER:
        while (true) {
            String op = Util.leerString(menu);
            switch (op) {
                case "i":
                    arreglo = log.crearArreglo(5, 10);
                    System.out.println(log.imprimir(arreglo));
                    arreglo = log.crearArreglo(10, 5);
                    System.out.println(log.imprimir(arreglo));
                    break;
                case "ii":
                    System.out.println(log.sumarImpares(3, arreglo));
                    break;
                case "iii":
                    System.out.println(log.imprimir(annos));
                    System.out.println(log.cantidadBisiestos(annos));
                    break;
                case "iv":
                    break OUTER;
                default:
                    System.out.println("Opción Inválida");
                    ;
            }
        }

        Persona[] personas = new Persona[10];
        personas[0] = new Persona(1, "Allan", "Murillo", "Alfaro", "allanmual@gmail.com", "8526-2638");
        personas[1] = new Persona(2, "Roberto", "Murillo", "Alfaro", "rmurillo@gmail.com", "7526-2638");

        for (Persona persona : personas) {
            if (persona != null) {
                System.out.println(persona.obtenerNombre());
            }
        }

    }

}
