/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class Arreglos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] numeros = new int[5];
        System.out.println(numeros[4]);
        numeros[4] = 10;
        System.out.println(numeros[4]);
        System.out.println("Largo: " + numeros.length);

        String[] dias = {"Domingo", "Lunes", "Martes",
            "Miércoles", "Jueves", "Viernes", "Sábado"};

        //Imprimir Datos
        for (int i = 0; i < dias.length; i++) {
            System.out.println(dias[i]);
        }

        //Leer Datos
        for (int i = 0; i < dias.length; i++) {
            dias[i] = "Leer dato";
        }

        Usuario[] usuarios = new Usuario[20];
        usuarios[0] = new Usuario(132, "Santo");
        
        String correo = "asdas@asd.com";
        for (int i = 0; i < correo.length(); i++) {
            System.out.println(correo.charAt(i));
        }

    }

}
