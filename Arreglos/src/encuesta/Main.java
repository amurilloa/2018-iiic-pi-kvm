/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        log.agregarFormularios(new Formulario(123, 1, 1, 350));
        log.agregarFormularios(new Formulario(124, 2, 1, 250));
        log.agregarFormularios(new Formulario(125, 1, 1, 350));
        log.agregarFormularios(new Formulario(126, 2, 1, 250));
        log.agregarFormularios(new Formulario(127, 2, 1, 150));
        log.agregarFormularios(new Formulario(128, 2, 1, 650));
        log.agregarFormularios(new Formulario(129, 1, 1, 550));
        log.agregarFormularios(new Formulario(130, 1, 1, 250));
        log.agregarFormularios(new Formulario(131, 2, 2));
        log.agregarFormularios(new Formulario(132, 2, 2));
        log.agregarFormularios(new Formulario(133, 1, 2));
        log.agregarFormularios(new Formulario(134, 1, 2));
        log.agregarFormularios(new Formulario(135, 2, 2));
        log.agregarFormularios(new Formulario(136, 1, 2));
        log.agregarFormularios(new Formulario(137, 2, 2));
        log.agregarFormularios(new Formulario(138, 1, 2));
        log.agregarFormularios(new Formulario(139, 1, 2));

        System.out.println(String.format("Hombres: %.0f%s", log.porcentaje(1), "%"));
        System.out.println(String.format("Mujeres: %.0f%s", log.porcentaje(2), "%"));
        System.out.println(String.format("Hombres Trab.: %.0f%s", log.porcentajeTrabajan(1), "%"));
        System.out.println(String.format("Mujeres Trab.: %.0f%s", log.porcentajeTrabajan(2), "%"));
        System.out.println(String.format("Sueldo Prom. Hombres.: $%.0f", log.sueldoProm(1)));
        System.out.println(String.format("Sueldo Prom. Mujeres.: $%.0f", log.sueldoProm(2)));
    }
}
