/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Formulario[] encuesta;

    public Logica() {
        encuesta = new Formulario[50];
    }

    public void agregarFormularios(Formulario form) {
        for (int i = 0; i < encuesta.length; i++) {
            if (encuesta[i] == null) {
                encuesta[i] = form;
                break;
            }
        }
    }

    public double porcentaje(int sexo) {
        int total = 0;
        int totalSex = 0;
        for (Formulario form : encuesta) {
            if (form != null) {
                total++;
                if (form.getSexo() == sexo) {
                    totalSex++;
                }
            }
        }
        return totalSex * 100.0 / total;
    }

    public double porcentajeTrabajan(int sexo) {
        int total = 0;
        int totalTrab = 0;

        for (Formulario form : encuesta) {
            if (form != null) {
                if (form.getSexo() == sexo) {
                    total++;
                    if (form.getTrabaja() == 1) {
                        totalTrab++;
                    }
                }
            }
        }
        return totalTrab * 100.0 / total;

    }

    public double sueldoProm(int sexo) {
        int sueldoTotal = 0;
        int totalTrab = 0;

        for (Formulario form : encuesta) {
            if (form != null) {
                if (form.getSexo() == sexo) {
                    if (form.getTrabaja() == 1) {
                        totalTrab++;
                        sueldoTotal += form.getSueldo();
                    }
                }
            }
        }
        return (double) sueldoTotal / totalTrab;
    }
}
