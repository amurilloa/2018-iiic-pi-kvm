/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Perro[] perros;

    public Logica() {
        perros = new Perro[25];
    }

    public void capturarPerro(Perro perro) {
        for (int i = 0; i < perros.length; i++) {
            if (perros[i] == null) {
                perros[i] = perro;
                break;
            }
        }
    }

    public String reporte() {
        String str = "";
        for (Perro perro : perros) {
            if (perro != null) {
                str += perro.info() + "\n";
            }
        }
        return str;
    }

    public Perro bucarPerro(String nombre, String raza) {
        for (Perro p : perros) {
            if (p != null && nombre.equals(p.getNombre())
                    && raza.endsWith(p.getRaza())) {
                return p;
            }
        }
        return null;
    }

}
