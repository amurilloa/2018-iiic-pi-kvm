/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author ALLAN
 */
public class Perro {

    private String nombre;
    private String raza;
    private boolean pedigree;

    public Perro() {
    }

    public Perro(String nombre, String raza, boolean pedigree) {
        this.nombre = nombre;
        this.raza = raza;
        this.pedigree = pedigree;
    }

    public String info() {
        return nombre + " - " + raza;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public boolean isPedigree() {
        return pedigree;
    }

    public void setPedigree(boolean pedigree) {
        this.pedigree = pedigree;
    }

}
