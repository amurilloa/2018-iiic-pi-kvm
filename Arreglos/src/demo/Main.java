/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        log.capturarPerro(new Perro("Santa", "Salchicha", true));
        Perro p1 = new Perro("Firulais", "Perrillo", false);
        log.capturarPerro(p1);
        System.out.println(log.reporte());

        String nombre = "Firulais";
        String raza = "Salchicha";

        Perro p = log.bucarPerro(nombre, raza);
        if (p == null) {
            System.out.println("No tenemos ese perro registrado");
        } else if (p.isPedigree()) {
            System.out.println("Inyección de 10 mil");
        } else {
            System.out.println("Inyección de 2 mil");
        }

    }
}
