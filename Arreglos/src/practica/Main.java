/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        int[] numeros = {10, 3, 4, 1, 9, 6, 7, 3, 8, 1, 5};
        Practica p = new Practica(numeros);
        System.out.println("Arreglo: " + p.imprimir());
        String pro = String.format("Promedio: %.2f", p.promedio());
        System.out.println(pro);
        String may = String.format("Mayor: %d", p.mayor());
        System.out.println(may);
        int elemento = 5;
        boolean b = p.buscar(elemento);
        String bus = String.format("Buscar: El %d %s está en el arreglo",
                elemento, b ? "sí" : "no");
        System.out.println(bus);
        if (b) {
            String busP = String.format("Posición: El %d está en la posición %d",
                    elemento, p.buscarPos(elemento) + 1);
            System.out.println(busP);
        }
        p.invertir();
        System.out.println("Invertir: " + p.imprimir());
        p.rotar();
        System.out.println("Rotar: " + p.imprimir());

    }
}
