/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author ALLAN
 */
public class Practica {

    private int[] arreglo;

    public Practica(int[] arreglo) {
        this.arreglo = arreglo;
    }

    public double promedio() {
        double suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        return suma / arreglo.length;
    }

    public int mayor() {
        if (arreglo.length > 0) {
            int mayor = arreglo[0];
            for (int i = 0; i < arreglo.length; i++) {
                if (arreglo[i] > mayor) {
                    mayor = arreglo[i];
                }
            }
            return mayor;
        } else {
            return 0;
        }
    }

    public boolean buscar(int elemento) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento) {
                return true;
            }
        }
        return false;
    }

    public int buscarPos(int elemento) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento) {
                return i;
            }
        }
        return -1;
    }

    public String imprimir() {
        String txt = "";
        for (int i = 0; i < arreglo.length; i++) {
            txt += arreglo[i] + ", ";
        }
        return txt + "\b\b";
    }

    public void invertir() {
        int[] temp = new int[arreglo.length];
        int j = 0;
        for (int i = arreglo.length - 1; i >= 0; i--) {
            temp[j] = arreglo[i];
            j++;
        }
        arreglo = temp;
    }

    public void rotar() {
        int ult = arreglo[arreglo.length - 1];
        for (int i = arreglo.length - 1; i > 0; i--) {
            arreglo[i] = arreglo[i - 1];
        }
        arreglo[0] = ult;
    }
}
